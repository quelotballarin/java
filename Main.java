import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Main {

	public static void main(String[] args) {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		tmb(br);
	}

	private static void tmb(BufferedReader br) {
		boolean error = false;

		String name = null;
		int age = null;
		double weight = null;
		double height = null;
		String sex = null;
		int daysPerWeek = null;

		double tmb = null;

		if (br != null) {
			try {
				do {
					System.out.println("Introdueix el teu nom: ");
					name = br.readLine();

					System.out.println("Introdueix la teva edat: ");
					age = Integer.parseInt(br.readLine());

					System.out.println("Introdueix el teu pes: (en kg)");
					weight = Double.parseDouble(br.readLine());

					System.out.println("Introdueix la teva altura: (en cm)");
					height = Double.parseDouble(br.readLine());

					System.out.println("Introdueix el teu sexe (H / D): ");
					sex = br.readLine();

					if (sex != null && !sex.toUpperCase().equals(Sex.MALE) || !sex.toUpperCase().equals(Sex.FEMALE)) {
						System.out.println("Sexe no vàlid");
						error = true;
					}

					System.out.println("Quants dies a la setmana fas exercici?");
					daysPerWeek = Integer.parseInt(br.readLine());

					if (daysPerWeek != null && daysPerWeek < 0 || daysPerWeek > 7) {
						System.out.println("Els dies per setamana ha de ser un valor entre 0 i 7");
						error = true;
					}
				} while (error);

				switch (sex.toUpperCase()) {
					case Sex.MALE:
						tmb = ((10 * weight) + (6.25 * height) - (5 * age) + 5);
						break;
					case Sex.FEMALE:
						tmb = ((10 * weight) + (6.25 * height) - (5 * age) - 161);
						break;
					default:
						tmb = null;
				}

				if (age != null && weight != null && height != null) {
					if (daysPerWeek == 0) {
						tmb *= 1.2;
					} else if (daysPerWeek >= 1 && daysPerWeek <= 3) {
						tmb *= 1.375;
					} else if (daysPerWeek >= 3 && daysPerWeek <= 5) {
						tmb *= 1.55;
					} else if (daysPerWeek >= 5 && daysPerWeek <= 6) {
						tmb *= 1.725;
					} else {
						tmb *= 1.9; /* tots el dies de la setmana */
					}

					System.out.println(name
							+ ", la teva tassa metabòlica bassal en funció de les dades introduides és de: "
							+ Math.floor(tmb) + " KCAL (diaries)");
				}
			} catch (IOException e) {
				System.out.println("S'ha produit un error al llegir les teves dades. Torna-ho a intentar més tard");
				error = true;
			} catch (NumberFormatException e) {
				System.out.println("L'edat, l'altura, el pes i els dies per setmana han de ser valors numèrics");
				error = true;
			} finally {
				try {
					br.close();
					System.exit(0);
				} catch (IOException | SecurityException e) { }
			}
		}
	}
}

public enum Sex {
	MALE = "M",
	FEMALE = "D"
}

